(module interp (lib "eopl.ss" "eopl")
  

;  (require "drscheme-init.scm")
  (require "lang.scm")
  (require "environments.scm")

  (provide translate-rule)


  ;; translate-rule: Rule -> Code
  ;;     generates C++ code for the translation rule
  
  (define translate-rule
    (lambda (this-rule)
      (reset-name)
      (cases rule this-rule 
        (a-rule 
          (name lhs-expr rhs-expr)
          ; generate procedure open
          (let ((node (get-newname))
                (expr (get-newname)))
            (eopl:printf "SgNode* rule_~a(SgNode *node~a)~n{~n" name node)
            (eopl:printf "    SgExpression *node~a = isSgExpression(node~a);~n" expr node)
            (let* ((env (translate-test lhs-expr node (empty-env)))
                   (replace-name (translate-replace rhs-expr env))
                   (temp-name (get-newname)))
              (eopl:printf "    #ifdef DEBUG~n")
              (eopl:printf "    cout << \"Applying rule_~a\" << endl;~n" name)
              (eopl:printf "    #endif~n")
              (eopl:printf "    replaceExpression(node~a, node~a);~n" expr replace-name)
              (eopl:printf "    SgNode *node~a = isSgNode(node~a);~n" temp-name replace-name)
              (eopl:printf "    return node~a;~n" temp-name)))
              ; generate procedure close
              (eopl:printf "}~n"))))) 
 
   ;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

  ;; translate-test-no: 
  ;;   generate code for TEST part of the rule for an expression with no
  ;;   operands (const, term).
  (define translate-test-no 
    (lambda (type id node env)
      (let ((name (get-newname)))
        (eopl:printf "    ~a *node~a = is~a(node~a);~n" type name type node)
        (eopl:printf "    if (node~a == NULL) return NULL; ~n" name)
        (extend-env id name env)))) 

  ;; translate-test-unary
  ;;   generate code for TEST part of the rule for an expression with one
  ;;   operand
  (define translate-test-unary
    (lambda (type opd-exp node env)
      (let ((oper-name (get-newname)))
        (eopl:printf "    ~a *node~a = is~a(node~a);~n" type oper-name type node)
        (eopl:printf "    if (node~a == NULL) return NULL; ~n" oper-name )
        ; translate  expr
        (let ((opd-name (get-newname)))
          (eopl:printf "    SgExpression *node~a = node~a->get_operand();~n" opd-name oper-name)
          (translate-test opd-exp opd-name env))))) 

  ;; translate-test-binary: 
  ;;   generate code for TEST part of the rule for a binary operation.
  (define translate-test-binary 
    (lambda (type lhs-exp rhs-exp node env)
      (let ((oper-name (get-newname)))
        (eopl:printf "    ~a *node~a = is~a(node~a);~n" type oper-name type node)
        (eopl:printf "    if (node~a == NULL) return NULL; ~n" oper-name)
        ; translate left expr
        (let ((lhs-name (get-newname)))
          (eopl:printf "    SgExpression *node~a = node~a->get_lhs_operand();~n" lhs-name oper-name)
          (let  ((env2 (translate-test lhs-exp lhs-name env))
                 (rhs-name (get-newname)))
              (eopl:printf "    SgExpression *node~a = node~a->get_rhs_operand();~n" rhs-name oper-name)
              (translate-test rhs-exp rhs-name env2))))))

  ;; translate-test: Expression x NodeName x Env -> Code x Env
  ;;   generates code for the TEST part of the rule
  ;;   Expression is the root expression to match
  ;;   NodeName is the internal name for the top node
  ;;   Env gives the bindings for C++ names of constant and terms generated
  ;;   Besides outputing the code, the procedure returns the updated enivronment
  ;;   with bindings for the identifiers in the Expression tree
  (define translate-test
    (lambda (exp node env)
      (cases expression exp
        (const-term (id)
        (translate-test-no 'SgIntVal id node env))
        ; get the C++ name of the term; no code to generate
        (t-term 
          (id)
          (extend-env id node env))
        ; generate code to test for an add-expression
        (addop-exp (lhs-exp rhs-exp)
          (translate-test-binary 'SgAddOp lhs-exp rhs-exp node env))
        ; generate code to test for a subtraction-expression
        (subtractop-exp 
          (lhs-exp rhs-exp)
          (translate-test-binary 'SgSubtractOp lhs-exp rhs-exp node env))
        ; generate code to test for a mult-expression
        (multiplyop-exp
          (lhs-exp rhs-exp)
          (translate-test-binary 'SgMultiplyOp lhs-exp rhs-exp node env))
        ; generate code to test for a unarysub-expression
        ; TODO Fix this.  A test needs to be added to make sure the minus op
        ; is with a constant.
        (minusop-exp 
          (opd-exp)
          (translate-test-unary 'SgMinusOp opd-exp node env))
        ; generate code to test whether node is a specific literal constant
        (value-exp (val)
          (let ((name (get-newname))
            (val-name (get-newname)))
            (eopl:printf "    SgIntVal * node~a = isSgIntVal(node~a); ~n" name node)
            (eopl:printf "    if (node~a == NULL) return NULL; ~n" name )
            (eopl:printf "    int node~a = node~a->get_value();~n" val-name name)
            (eopl:printf "    if (node~a != ~a) return NULL;~n" val-name val)))
        ; catch errors -- eval-exp is not acceptable in LHS
        (eval-exp (exp)
          (eopl:error "Unacceptable use of eval-exp ~a in LHS of a rule" exp))
        (id-exp (id)
          (eopl:error "Unacceptable use of id-exp ~a in LHS of a rule" id)))))

  ;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  ;; print-build-binary
  ;;   Print the C++ code to build a node that takes two expressions
  (define print-build-binary
    (lambda (type lhs-exp rhs-exp env)
      (let ((lhs-name (translate-replace lhs-exp env))
            (rhs-name (translate-replace rhs-exp env))
            (oper-name (get-newname)))
        (eopl:printf "    Sg~a *node~a = build~a(node~a, node~a);~n" type oper-name type lhs-name rhs-name)
        oper-name)))

  ;; translate-replace: Expression x Env -> Code x NodeName
  ;;    generates code for the RHS of a rule -- the tree generated for replacement
  ;;    Expression gives the RHS
  ;;    Env is carried from the LHS; it gives the C++ names of nodes in the LHS
  ;;    Besides generating code, the procedure returns a NodeName
  ;;     which gives the C++ name of the variable holding the generated expression
  
  (define translate-replace
    (lambda (exp env)
      (cases expression exp
        ; eval-exp --> transfer to translate-eval for generating evaluation code
        (eval-exp (opd-exp)
          (let ((val-name (translate-eval opd-exp env))
            (node-name (get-newname)))
            (eopl:printf "    SgIntVal *node~a = buildIntVal(node~a);~n" node-name val-name)
            node-name))
        ; lit-exp -> generate code for creating a literal AST node
        (value-exp (val)
          ;(print-build-unary 'IntVal val env)
          (let ((node-name (get-newname)))
            (eopl:printf "    SgIntVal *node~a = buildIntVal(~a);~n" node-name val)
            node-name))
        ; generate code for creating an add-expression
        (addop-exp
          (lhs-exp rhs-exp)
          (print-build-binary 'AddOp lhs-exp rhs-exp env))
        ; generate code for creating an multiply-expression
        (multiplyop-exp
          (lhs-exp rhs-exp)
          (print-build-binary 'MultiplyOp lhs-exp rhs-exp env))
        ; generate code for creating a unary-subtract expression
        (minusop-exp
          (opd-exp)
          (let ((opd-name (translate-replace opd-exp env)))
            (let ((oper-name (get-newname)))
              (eopl:printf "    SgMinusOp *node~a = buildMinusOp(node~a);~n" oper-name opd-name)
              oper-name)))
        ; generate code for creating an sub-expression
        (subtractop-exp
          (lhs-exp rhs-exp)
            (print-build-binary 'SubtractOp lhs-exp rhs-exp env))
        ; generate code for cloning an constant in the LHS
        (const-term
          (id)
          (let ((t-name (apply-env env id))
                (clone-name (get-newname)))
            (eopl:printf "    SgExpression *node~a = copy_expr(node~a);~n" clone-name t-name)
            clone-name))
          ;(apply-env env id))
        ;; generate code for creating a clone of term in the LHS
        (t-term
          (id)
          (let ((t-name (apply-env env id))
                (clone-name (get-newname)))
            (eopl:printf "    SgExpression *node~a = copy_expr(node~a);~n" clone-name t-name)
            clone-name))
          ;(apply-env env id))
        ; error cases
        (id-exp (id)
          (eopl:error "Unacceptable use of id-exp ~a in RHS of a rule" id)))))
  
  ;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ;; translate-eval: Expression x Env -> Code x NodeName
  ;;   Generates code for evaluating expression in the generated code itself
  ;;   Expression gives the expression to be evaluated
  ;;      its leaf should only be constant-term -- pointing to constants in the LHS of rules
  ;;   Env gives the bindings for identifiers in the rules to C++ names
  ;;   The procedure generates code for evaluating expression and alse
  ;;   returns the name of the C++ variable holding the evaluated value
  
  (define translate-eval
    (lambda (exp env)
      (cases expression exp
        ; generate code for performing "+" in the C++ code 
        (addop-exp (lhs-exp rhs-exp)
          (let ((lhs-name (translate-eval lhs-exp env))
                (rhs-name (translate-eval rhs-exp env))
                (oper-name (get-newname)))
            (eopl:printf "    int node~a = node~a + node~a;~n" oper-name lhs-name rhs-name)
            oper-name))
        ; generate code for performing "*" in the C++ code
        (multiplyop-exp (lhs-exp rhs-exp)
          (let ((lhs-name (translate-eval lhs-exp env))
                (rhs-name (translate-eval rhs-exp env))
                (oper-name (get-newname)))
             (eopl:printf "    int node~a = node~a * node~a;~n" oper-name lhs-name rhs-name)
             oper-name))
        ; generate code for performing unary "-" in the C++ code
        (minusop-exp (lhs-exp)
          (let ((lhs-name (translate-eval lhs-exp env))
                (oper-name (get-newname)))
            (eopl:printf "    int node~a = -node~a;~n" oper-name lhs-name)
            oper-name))
        ; generate code for performing binary "-" in the C++ code(sub-exp
        (subtractop-exp (lhs-exp rhs-exp)
         (let ((lhs-name (translate-eval lhs-exp env))
               (rhs-name (translate-eval rhs-exp env))
               (oper-name (get-newname)))
           (eopl:printf "    int node~a = node~a - node~a;~n" oper-name lhs-name rhs-name)
           oper-name))
        ; generate code to assign literal value to a C++ var
        (value-exp (val)
          (let ((lit-name (get-newname)))
            (eopl:printf "    int node~a = node~a" lit-name val)
            lit-name))
        ; generate code to fetch value of an constant in the C++ code
        (const-term (id)
          (let ((const-name (get-newname)))
            (eopl:printf "    int node~a = node~a->get_value();~n" const-name (apply-env env id))
            const-name))
        ; error handlers
        (eval-exp (opd-exp)
          (eopl:error "Cannot evaluate an eval-expression"))
        (t-term (id)
          (eopl:error "Cannot have t-term in eval() expression"))
        (id-exp (id)
          (eopl:error "Unacceptable use of id-exp ~a in eval-exp in RHS of a rule" id)))))
 
  ;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ;; Support code -- used to generate names for variables
  ;;  This code really does not generate names. It generates numbers
  ;;  which are converted to names during code generation (a la, printing)
  ;;  Its just a work around. I'd have preferred to generate symbols. But didn't have
  ;;  patience to figure out the details of how to do it.
  (define count 0)
  (define (reset-name) (set! count 0))
  (define (get-newname)
    (set! count (+ count 1))
    count)
  
)

