(module data-structures (lib "eopl.ss" "eopl")

  ;; data structures for let-lang.

  (provide (all-defined))               ; too many things to list

;;;;;;;;;;;;;;;; expressed values ;;;;;;;;;;;;;;;;
; In ROSE, all constants of the form [0-9]+ are integers and
; all constants of the form [0-9]+.[0-9]+ are doubles
  
;  Rule ::=   Id:  expr => expr
;  expr::=    "const" : id      // Integer value
;             | "t" : id    // Term (not a constant)
;             | - expr:     // MinusOp 
;             | value       // Constant Value
;             | expr + expr // AddOp
;             | expr - expr // SubtractOp
;             | expr * expr // MultipyOp
;             | (expr)      // Grouping
;             | eval(expr)  // Only used on RHS
  
  (define identifier? symbol?)
  
  (define-datatype rule rule?
    (a-rule
     (name identifier?)
     (lhs-expr expression?)
     (rhs-expr expression?)
    ))
  
  (define-datatype expression expression?
    (addop-exp 
     (lhs-expr expression?)
     (rhs-expr expression?)
     )
    (subtractop-exp
     (lhs-expr expression?)
     (rhs-expr expression?)
     )
    (multiplyop-exp 
     (lhs-expr expression?)
     (rhs-expr expression?)
     )
    (minusop-exp 
     (opd-expr expression?)
     )
    ; used only in rules, both LHS and RHS
    (const-term (id identifier?))
    ; used only in rules, both LHS and RHS, but not eval
    (t-term (id identifier?))
    ; used only in RHS, but not nested in eval
    (eval-exp
     (opd-expr expression?)
     )
    ; used in actual expression - not in the rules
    (value-exp
     (value number?)
     )
    (id-exp
     (name identifier?))
    )
 
  (define (print-rule r)
    (cases rule r
      (a-rule (name lhs-expr rhs-expr)
              (eopl:printf "~a " name)
              (print-expr lhs-expr)
              (eopl:printf " => " )
              (print-expr rhs-expr)
              (eopl:printf "~n")
              (string->symbol (string-append "rule_" (symbol->string name)))
              )
      )
    )


  (define (print-expr exp)
    (cases expression exp
      (const-term (id) 
                  (eopl:printf "const:~a" id))
      (t-term (id) 
              (eopl:printf "t:~a" id))
      (eval-exp (oper-exp)
                (eopl:printf "(eval ")
                (print-expr oper-exp)
                (eopl:printf ")"))
      (addop-exp (lhs-expr rhs-expr)
               (eopl:printf "(")
               (print-expr lhs-expr)
               (eopl:printf " + ")
               (print-expr rhs-expr)
               (eopl:printf ")"))
      (subtractop-exp (lhs-expr rhs-expr)
               (eopl:printf "(")
               (print-expr lhs-expr)
               (eopl:printf " - ")
               (print-expr rhs-expr)
               (eopl:printf ")"))
      (multiplyop-exp (lhs-expr rhs-expr)
               (eopl:printf "(")
                (print-expr lhs-expr)
                (eopl:printf " * ")
                (print-expr rhs-expr)
               (eopl:printf ")"))
      (minusop-exp (lhs-expr)
                    (eopl:printf "(- ")
                    (print-expr lhs-expr)
                    (eopl:printf ")"))
      (value-exp (val)
               (eopl:printf "~a" val))
      ;(dvalue-exp (val)
      ;         (eopl:printf "~a" val))
      (id-exp (id)
              (eopl:printf "~a" id))
      )
    )
  
  ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    ; rule id exp => exp
  
  (define (parse-rule data)
    (and (list? data)
         (eq? (length data) 5)
         (eq? (car data) 'rule)
         (let ((id (cadr data))
               (lhs (caddr data))
               (arr (cadddr data))
               (rhs (cadddr (cdr data))))
           (a-rule id (parse-exp lhs) (parse-exp rhs))
           )
    )
    )
  
;  expr::=    "const" : id      // Integer value

  (define parse-const-term
    (lambda (sym)
      (let ((str (symbol->string sym)))
        (if (and (> (length (string->list str)) 5)
                 (eq? #\: (string-ref str 5)))
            (const-term (string->symbol (substring str 6)))
            #f)
      )))

;             | "t" : id    // Term (not an int or double)

  (define parse-t-term
    (lambda (sym)
      (let ((str (symbol->string sym)))
        (if (and (> (length (string->list str)) 2)
                 (eq? #\: (string-ref str 1)))
            (t-term (string->symbol (substring str 2)))
            #f)
      )))

;             | - expr:     // MinusOp 
;             | value       // A literal value (number)
;             | expr + expr // AddOp
;             | expr - expr // SubtractOp
;             | expr * expr // MultipyOp
;             | (expr)      // Grouping
;             | eval(expr)  // Only used on RHS

  (define (parse-exp data)
    (cond 
      ((not (list? data))
       (cond
         ((number? data) (value-exp data))
         ((parse-const-term data))
         ((parse-t-term data))
         ((identifier? data) (id-exp data))
         (#t (eopl:error "Parse error ~a" data))))
      (#t (let ((fst (car data))
                (len (length data)))
            (cond 
              ((eq? len 2)
               (cond ((eq? fst 'const)
                      (const-term (cadr data)))
                     ((eq? fst 't)
                      (t-term (cadr data)))
                     ((eq? fst 'eval)
                      (eval-exp (parse-exp (cadr data))))
                     ((eq? fst '-)
                      (minusop-exp (parse-exp (cadr data))))
                     (#t (eopl:error "parse error ~a" data))))
              ((eq? len 3)
               (let ((lhs (parse-exp (car data)))
                     (op (cadr data))
                     (rhs (parse-exp (caddr data))))
                 (cond
                   ((eq? op '+) (addop-exp lhs rhs))
                   ((eq? op '-) (subtractop-exp lhs rhs))
                   ((eq? op '*) (multiplyop-exp lhs rhs))
                   (#t (eopl:error "Parse error ~a" data)))
                   )
               )
              (#t (eopl:error "Parse error ~a" data)))
            
            ))
      )
    )
   
;;;;;;;;;;;;;;;; environment structures ;;;;;;;;;;;;;;;;

;; example of a data type built without define-datatype

  (define empty-env-record
    (lambda () 
      '()))

  (define extended-env-record
    (lambda (sym val old-env)
      (cons (list sym val) old-env)))
  
  (define empty-env-record? null?)
  
  (define environment?
    (lambda (x)
      (or (empty-env-record? x)
          (and (pair? x)
               (symbol? (car (car x)))
               (expression? (cadr (car x)))
               (environment? (cdr x))))))

  (define extended-env-record->sym
    (lambda (r)
      (car (car r))))

  (define extended-env-record->val
    (lambda (r)
      (cadr (car r))))

  (define extended-env-record->old-env
    (lambda (r)
      (cdr r)))
)
