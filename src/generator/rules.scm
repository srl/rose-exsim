#lang racket
(require "lang.scm")

(provide all-rules)

;; rules taken from Muchnick, Advanced Compiler Design, pages 337,338
  
(define rule_r1 '(rule r1 (const:c1 + const:c2) => (eval (const:c1 + const:c2))))
(define rule_r2 '(rule r2 (t:t1 + const:c1) => (const:c1 + t:t1)))
(define rule_r3 '(rule r3 (const:c1 * const:c2) => (eval (const:c1 * const:c2))))
(define rule_r4 '(rule r4 (t:t1 * const:c1) => (const:c1 * t:t1)))
(define rule_r5 '(rule r5 (const:c1 - const:c2) => (eval (const:c1 - const:c2))))
(define rule_r6 '(rule r6 (t:t1 - const:c1) => ((- const:c1) + t:t1)))
(define rule_r7 '(rule r7 (t:t1 + (t:t2 + t:t3)) => ((t:t1 + t:t2) + t:t3)))
(define rule_r8 '(rule r8 (t:t1 * (t:t2 * t:t3)) => ((t:t1 * t:t2) * t:t3)))
(define rule_r9 '(rule r9 ((const:c1 + t:t1) + const:c2) => ((eval (const:c1 + const:c2)) + t:t1)))
(define rule_r10 '(rule r10 ((const:c1  * t:t1) * const:c2) => ((eval (const:c1 * const:c2)) * t:t1)))
(define rule_r11 '(rule r11 ((const:c1 + t:t1) * const:c2) => ((eval (const:c1 * const:c2)) + (const:c2 * t:t1))))
(define rule_r12 '(rule r12 (const:c1 * (const:c2 + t:t1)) => ((eval (const:c1 * const:c2)) + (const:c1 * t:t1))))
(define rule_r13 '(rule r13 ((t:t1 + t:t2) * const:c1) => ((const:c1 * t:t1) + (const:c1 * t:t2))))
(define rule_r14 '(rule r14 (const:c1 * (t:t1 + t:t2)) => ((const:c1 * t:t1) + (const:c1 * t:t2))))
(define rule_r15 '(rule r15 ((t:t1 - t:t2) * const:c1) => ((const:c1 * t:t1) - (const:c1 * t:t2))))
(define rule_r16 '(rule r16 (const:c1 * (t:t1 - t:t2)) => ((const:c1 * t:t1) - (const:c1 * t:t2))))
(define rule_r17 '(rule r17 ((t:t1 + t:t2) * t:t3)  => ((t:t1 * t:t3) + (t:t2 * t:t3))))
(define rule_r18 '(rule r18 (t:t1 * (t:t2 + t:t3))  => ((t:t1 * t:t2) + (t:t1 * t:t3))))
(define rule_r19 '(rule r19 ((t:t1 - t:t2) * t:t3)  => ((t:t1 * t:t3) - (t:t2 * t:t3))))
(define rule_r20 '(rule r20 (t:t1 * (t:t2 - t:t3))  => ((t:t1 * t:t2) - (t:t1 * t:t3))))

;; added these rules, were not in Muchnick's book
(define rule_r21 '(rule r21 (- const:c1) => (eval (- const:c1))))
(define rule_r22 '(rule r22 (1 * t:t1) => t:t1))
(define rule_r23 '(rule r23 (0 * t:t1) => 0))

; list of all the rules
(define all-rules (map parse-rule (list rule_r1 rule_r2 rule_r3 rule_r4 rule_r5 rule_r6 rule_r7 rule_r8 rule_r9 rule_r10
                                        rule_r11 rule_r12 rule_r13 rule_r14 rule_r15 rule_r16 rule_r17 rule_r18 rule_r19 rule_r20
                                        rule_r21 rule_r22 rule_r23)))

