#!/usr/bin/env racket
;(module top (lib "eopl.ss" "eopl")
#lang eopl
  ;; top level module.  Loads all required pieces.
  ;; Run the test suite with (run-all).

;(require "drscheme-init.scm")
(require "lang.scm")           
(require "translate.scm") 
(require "simplify.scm")
(require "rules.scm")

;;;  toplevel driver  for generating C++ code for one rule
(define (run-trans rule)
  (eopl:printf "~n//")
  (print-rule rule)
  (translate-rule rule))

(define (trans-all lst)
  (cond ((null? lst))
        (#t (run-trans (car lst))
            (trans-all (cdr lst)))))

(define (write-all lst)
  (write-header)
  (trans-all lst)
  (write-footer lst))

(define (write-header)
  (eopl:printf "/*~n")
  (eopl:printf " * AST Simplification Rules Implementation~n")
  (eopl:printf " */~n")
  (eopl:printf "#include \"rose.h\"~n")
  (eopl:printf "#include <iostream>~n")
  (eopl:printf "using namespace SageBuilder;~n")
  (eopl:printf "using namespace SageInterface;~n")
  (eopl:printf "using namespace std;~n")
  (eopl:printf "~ntypedef SgNode* (*p_rulefunc)(SgNode*);~n")
  (eopl:printf "~nSgExpression* copy_expr(SgExpression *n) {~n")
  (eopl:printf "    SgTreeCopy copy_class;~n")
  (eopl:printf "    return isSgExpression(n->copy(copy_class));~n}"))

(define (write-footer lst)
  (eopl:printf "int rule_count = ~a;~n" (length lst))
  (eopl:printf "p_rulefunc rule_funcs[~a] = {~n" (length lst))
  (eopl:printf "&rule_~a~n" (get-rule-name (car lst)))
  (print-list-tail (cdr lst))
  (eopl:printf "};~n"))

; Assign all the rules in lst to the pointer array rule_func
(define (print-list-tail lst)
  (cond ((null? lst))
        (#t (print-list-item (car lst))
            (print-list-tail (cdr lst)))))

;(define p (open-output-file "test.txt"))

(define (print-list-item item)
  (eopl:printf ",&rule_~a~n" (get-rule-name item)))

(define (get-rule-name r)
  (cases rule r
    (a-rule (name lhs-expr rhs-expr)
            name)))


;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

; toplevel driver for simplfying expressions

(define (simplify-one exp)
  (eopl:printf "-----------Simplifying ----------~n")
  (print-expr exp)
  (let ((res-exp (simplify-expr-ROSE exp)))
    (eopl:printf "~n  =>  ")
    (print-expr res-exp)
    (eopl:printf "~n")
    )
  )
(define simplify-all
  (lambda (explist)
  (cond ((null? explist))
        (#t (simplify-one (car explist))
            (simplify-all (cdr explist)))))

  )


;; Examples for simplifying expressions

(define exp1 10)
(define exp2 'a)
(define exp3 '(10 + 20))
(define exp4 '(a + 10))
(define exp5 '((10 + 30) + 10))
(define exp6 '((a + 20) + 10))
(define exp7 '((10 + 20) + a))

(define lo1 10)
(define hi1 100)
(define lo2 1)
(define hi2 20)
(define w 4)

(define exp8 `(((- ((,lo1 * ((,hi2 - ,lo2) + 1)) - ,lo2)) * ,w) + base_a))
(define exp9 `((((,hi2 - ,lo2) + 1) * (i * ,w)) + (j * ,w))) 
(define exp10 (list exp8 '+ exp9))

(define exp-list (map parse-exp (list exp1 exp2 exp3 exp4 exp5 exp6 exp7 exp8 exp9 exp10)))

;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
;  pretty-print the rules

(define print-all-rules
  (lambda (rules)
    (cond ((null? rules)
           '())
          (#t (cons 
               (print-rule (car rules)) 
               (print-all-rules (cdr rules)))))))




;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
;(eopl:printf "(define all-rules (map parse-rule ~a))~n" (cons 'list (print-all-rules all-rules)))

;(simplify-all exp-list)
;; generates C++ code for ALL the rules
;(trans-all all-rules)
(write-all all-rules)
