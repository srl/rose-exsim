(module interp (lib "eopl.ss" "eopl")
  
  
  (require "drscheme-init.scm")
  (require "lang.scm")
  (require "rules.scm")
  (require "environments.scm")
  
  (provide simplify-expr apply-rule simplify-expr-ROSE)
  
  ;; simplify-expr: Exp -> Exp
  ;;   Returns simplified expressions
  
  (define simplify-expr
    (lambda (exp)
      (simplify-expr-helper exp all-rules all-rules)))
  
  ;; simplify-expr-helper: Expression x Rules x AllRules -> Expression
  ;;    AllRules is the set of all rules
  ;;      its passed as param, instead of using the global
  ;;   Rules are the remaining rules to try
  ;;   Expression is the toplevel expression
  ;;   A rule may be applied anywhere in the subtree
  ;;   if any of the Rules is applied, simplify-expr
  ;;       recurses on AllRules
  ;;   it returns the simplified expression
  
  (define simplify-expr-helper
    (lambda (exp rest-rules all-rules)
      (cond ((null? rest-rules)
             ; no rule applied - cannot simplify any further
             exp)
            (#t 
             (let ((this-rule (car rest-rules))
                   (other-rules (cdr rest-rules)))
               ; apply one rule
               (let ((apply-result (traverse-and-apply-rule exp this-rule)))
                 ; if rule didn't apply, recurse others
                 (if (not apply-result)
                     (simplify-expr-helper exp other-rules all-rules)
                     ; if this rule did apply, start over with all the rules
                     (simplify-expr apply-result)
                     )
                 )
               )
             )
            )
      )
    )
  
  ;; traverse-and-apply-rule: Expression x Rule -> Expression (#f)
  ;;    recurses through Expression, looking for opportunity to apply the
  ;;    given rule. If an opportunity is found, the rule is applied
  ;;    and the simplified expression returned
  ;;    if no opportunity is found, a #f is returned.
  (define traverse-and-apply-rule
    (lambda (exp this-rule)
      (cases expression exp
        ; assume, no rule applied on value-exp and id-exp
        (value-exp (val) #f)
        (id-exp (id) #f)
        ; check if rule applies on any child
        (addop-exp (exp1 exp2)
                 (let ((left-res (traverse-and-apply-rule exp1 this-rule)))
                   (if left-res
                       ; it applied on left child, return new tree
                       (addop-exp left-res exp2)
                       ; else apply on right child
                       (let ((right-res (traverse-and-apply-rule exp2 this-rule)))
                         (if right-res
                             (addop-exp exp1 right-res)
                             ; didn't apply on both, try this node
                             (apply-rule exp this-rule))))))
        (subtractop-exp (exp1 exp2)
                 (let ((left-res (traverse-and-apply-rule exp1 this-rule)))
                   (if left-res
                       ; it applied on left child, return new tree
                       (subtractop-exp left-res exp2)
                       ; else apply on right child
                       (let ((right-res (traverse-and-apply-rule exp2 this-rule)))
                         (if right-res
                             (subtractop-exp exp1 right-res)
                             ; didn't apply on both, try this node
                             (apply-rule exp this-rule))))))
        (multiplyop-exp (exp1 exp2)
                 (let ((left-res (traverse-and-apply-rule exp1 this-rule)))
                   (if left-res
                       ; it applied on left child, return new tree
                       (multiplyop-exp left-res exp2)
                       ; else apply on right child
                       (let ((right-res (traverse-and-apply-rule exp2 this-rule)))
                         (if right-res
                             (multiplyop-exp exp1 right-res)
                             ; didn't apply on both, try this node
                             (apply-rule exp this-rule))))))
        (minusop-exp (exp1)
                 (let ((left-res (traverse-and-apply-rule exp1 this-rule)))
                   (if left-res
                       ; it applied on left child, return new tree
                       (minusop-exp left-res)
                       (apply-rule exp this-rule))))
        (else #f)
        )))
  
  ;; apply-rule: Expression x Rule -> Expression (or #f)
  ;;    checks if the Rule can be applied on the given (toplevel)
  ;;    Expression. If it can be, it applies and returns the
  ;;    result of substitution. Else returns #f
  (define apply-rule
    (lambda (exp this-rule)
      (cases rule this-rule
        (a-rule (id lhs-exp rhs-exp)
                ; test the rule
                (let ((env (test-rule exp lhs-exp (empty-env))))
                  ; if test succeeds get new expression
                  (cond 
                      (env
                       (eopl:printf "~nApplying rule ~a" id)
                       (make-expr rhs-exp env))
                      (else #f))
                  )
                )
        )
      )
    )
  
  ;; test-rule: Exp x LhsExp x Env -> Env
  ;    returns #f if test fails
  ;    a new environment otherwise
  ;    the environment has bindings for the const:c and t:tx terms
  ;    in the LHS. These are used for substitution in the RHS
  (define test-rule
    (lambda (exp lhs-exp env)
      (cases expression lhs-exp
        ; a term in lhs matches any term
        (t-term (id)
                (extend-env id exp env))
        ; const term matches a value-exp
        (const-term (id)
                    (cases expression exp
                      (value-exp (val)
                               (extend-env id exp env))
                      (else #f)))
        ; a value-exp matches a value-exp with same value
        (value-exp (val1)
                 (cases expression exp
                   (value-exp (val2)
                            (if (eq? val1 val2)
                                env
                                #f))
                   (else #f)))
        ; id exp is not permitted in lhs
        (id-exp (id) 
                (eopl:error "Cannot have id-exp in RHS ~a" exp))
        ; addop-exp matches addop-exp, recursively
        (addop-exp (lhsexp1 lhsexp2)
                 (cases expression exp
                   (addop-exp (exp1 exp2)
                            (let ((env1 (test-rule exp1 lhsexp1 env)))
                              (if env1
                                  (test-rule exp2 lhsexp2 env1)
                                  #f)
                              )
                            )
                   (else #f)))
        ; subtractop-exp matches subtractop-exp, recursively
        (subtractop-exp (lhsexp1 lhsexp2)
                 (cases expression exp
                   (subtractop-exp (exp1 exp2)
                            (let ((env1 (test-rule exp1 lhsexp1 env)))
                              (if env1
                                  (test-rule exp2 lhsexp2 env1)
                                  #f)
                              )
                            )
                   (else #f)))
        ; multiplyop-exp matches multiplyop-exp, recursively
        (multiplyop-exp (lhsexp1 lhsexp2)
                 (cases expression exp
                   (multiplyop-exp (exp1 exp2)
                            (let ((env1 (test-rule exp1 lhsexp1 env)))
                              (if env1
                                  (test-rule exp2 lhsexp2 env1)
                                  #f)
                              )
                            )
                   (else #f)))
        ; minusop-exp matches minusop-exp, recursively
        (minusop-exp (lhsexp1)
                 (cases expression exp
                   (minusop-exp (exp1)
                                 (test-rule exp1 lhsexp1 env))
                   (else #f)))
        (else #f)
        )
      )
    )
  
  ;; make-expr: RhsExp x Env -> Exp
  ;;   create an expression used for substituting a match found
  ;;   takes an Env and the RHS of a rule
  ;;   applies substitutions for const:cx and t:tx terms
  ;;   also evaluates any (eval..) terms
  ;;   returns the new expression
  (define make-expr
    (lambda (rhsexp env)
      (cases expression rhsexp
        (value-exp (val) rhsexp)
        (const-term (id)
                    (apply-env env id))
        (t-term (id)
                (apply-env env id))
        (addop-exp (exp1 exp2)
                 (addop-exp (make-expr exp1 env) (make-expr exp2 env)))
        (subtractop-exp (exp1 exp2)
                 (subtractop-exp (make-expr exp1 env) (make-expr exp2 env)))
        (multiplyop-exp (exp1 exp2)
                 (multiplyop-exp (make-expr exp1 env) (make-expr exp2 env)))
        (minusop-exp (exp1)
                 (minusop-exp (make-expr exp1 env)))
        (eval-exp (exp1)
                  (value-exp (evaluate-expr exp1 env)))
        (else
         (eopl:error "Incorrect term ~a in RHS exp~n" rhsexp)
         )
                 
        ) 
      )
    )
  ; helper function
  (define (value-exp->val exp)
    (cases expression exp
      (value-exp (val) val)
      (else (eopl:error "Not a value-exp")))
    )
  
  ;; evaluate-expr: RhsExp x Env -> Value
  ;;    Evaluates a (eval ..) term in the RHS
  ;;    returns the resulting Value
  (define evaluate-expr
    (lambda (rhsexp env)
      (cases expression rhsexp
        (value-exp (val) val)
        (const-term (id)
                    (value-exp->val (apply-env env id)))
        (t-term (id)
                (eopl:error "Cannot evaluate a t-term"))
        (addop-exp (exp1 exp2)
                 (+ (evaluate-expr exp1 env) (evaluate-expr exp2 env)))
        (subtractop-exp (exp1 exp2)
                 (- (evaluate-expr exp1 env) (evaluate-expr exp2 env)))
        (multiplyop-exp (exp1 exp2)
                 (* (evaluate-expr exp1 env) (evaluate-expr exp2 env)))
        (minusop-exp (exp1)
                 (- (evaluate-expr exp1 env)))
        (eval-exp (exp1)
                  (eopl:error "Cannot have eval-exp inside eval-exp"))
        (else
         (eopl:error "Incorrect term ~a in RHS exp~n" rhsexp)
         )
                 
        ) 
      )
    )
  
  
  ;'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ;; post-order-traversal: Expression x Op -> (list Res)
  ;;    where Op: Exp x list(Res) -> Res
  ;;  It applies Op in post-order. The results from applying Op to children
  ;;    are passed on to the parent node.
  ;; ** This operation is provided by ROSE, and need not be implemented in C++
  
  (define post-order-traversal
    (lambda (exp post-op)
      (cases expression exp
        ; assume, no rule applied on value-exp and id-exp
        (value-exp (val) (post-op exp '()))
        (id-exp (id) (post-op exp '()))
        ; check if rule applies on any child
        (addop-exp (exp1 exp2)
                 (let ((left-res (post-order-traversal exp1 post-op))
                       (right-res (post-order-traversal exp2 post-op)))
                   (post-op exp (list left-res right-res))))
        (subtractop-exp (exp1 exp2)
                 (let ((left-res (post-order-traversal exp1 post-op))
                       (right-res (post-order-traversal exp2 post-op)))
                   (post-op exp (list left-res right-res))))
        (multiplyop-exp (exp1 exp2)
                  (let ((left-res (post-order-traversal exp1 post-op))
                        (right-res (post-order-traversal exp2 post-op)))
                    (post-op exp (list left-res right-res))))
        (minusop-exp (exp1)
                      (let ((left-res (post-order-traversal exp1 post-op)))
                        (post-op exp (list left-res))))
        (else #f)
        )))
  
 ;''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  ; simplify-visitor: Exp x (List Children) -> Exp
  ;   gets an expression, its list of simplified children
  ;      produces a simplified expression
  ;   ** in C++ code the simplified children would have been replaced
  ;   ** in the tree. But in Scheme we are not doing destructive assignment
  ;   ** So in C++ you'd not need this function
  (define simplify-visitor 
    (lambda (exp children)
      (cases expression exp
        (value-exp (val) (simplify-fully exp))
        (id-exp (id) (simplify-fully exp))
        (addop-exp (exp1 exp2)
                 (simplify-fully (addop-exp (car children) (cadr children))))
        (subtractop-exp (exp1 exp2)
                 (simplify-fully (subtractop-exp (car children) (cadr children))))
        (multiplyop-exp (exp1 exp2)
                  (simplify-fully (multiplyop-exp (car children) (cadr children))))
        (minusop-exp (exp1)
                      (simplify-fully (minusop-exp (car children))))
        (else #f)
        )))
    
  
   (define simplify-fully
     (lambda (exp)
       (simplify-fully-x exp all-rules all-rules)
       )
     )
  
   (define simplify-fully-x
    (lambda (exp rest-rules all-rules)
      (cond ((null? rest-rules)
             ; no rule applied - cannot simplify any further
             exp)
            (#t 
             (let ((this-rule (car rest-rules))
                   (other-rules (cdr rest-rules)))
               ; apply one rule
               (let ((apply-result (apply-rule exp this-rule)))
                 ; if rule didn't apply, recurse others
                 (if (not apply-result)
                     (simplify-fully-x exp other-rules all-rules)
                     ; if this rule did apply, start over with 
                     ; post-order traversal
                     ; ** CAUTION -- calling post-order traversal on intermediate
                     ;   node
                     (post-order-traversal apply-result simplify-visitor)
                     )
                 )
               )
             )
            )
      )
    )

  ;; simplify-expr-ROSE: Exp -> Exp
  ;;  ** in ROSE, this correspond to the call for traversal from main()
  (define simplify-expr-ROSE
    (lambda (exp)
      (post-order-traversal exp simplify-visitor)
      ))
  
 
  
    
    )

