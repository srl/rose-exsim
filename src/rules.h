/*
 * AST Simplification Rules Implementation
 */
#include "rose.h"
#include <iostream>
using namespace SageBuilder;
using namespace SageInterface;
using namespace std;

typedef SgNode* (*p_rulefunc)(SgNode*);

SgExpression* copy_expr(SgExpression *n) {
    SgTreeCopy copy_class;
    return isSgExpression(n->copy(copy_class));
}
//r1 (const:c1 + const:c2) => (eval (const:c1 + const:c2))
SgNode* rule_r1(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgAddOp *node3 = isSgAddOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal *node5 = isSgIntVal(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node3->get_rhs_operand();
    SgIntVal *node7 = isSgIntVal(node6);
    if (node7 == NULL) return NULL; 
    int node8 = node5->get_value();
    int node9 = node7->get_value();
    int node10 = node8 + node9;
    SgIntVal *node11 = buildIntVal(node10);
    #ifdef DEBUG
    cout << "Applying rule_r1" << endl;
    #endif
    replaceExpression(node2, node11);
    SgNode *node12 = isSgNode(node11);
    return node12;
}

//r2 (t:t1 + const:c1) => (const:c1 + t:t1)
SgNode* rule_r2(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgAddOp *node3 = isSgAddOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgExpression *node5 = node3->get_rhs_operand();
    SgIntVal *node6 = isSgIntVal(node5);
    if (node6 == NULL) return NULL; 
    SgExpression *node7 = copy_expr(node6);
    SgExpression *node8 = copy_expr(node4);
    SgAddOp *node9 = buildAddOp(node7, node8);
    #ifdef DEBUG
    cout << "Applying rule_r2" << endl;
    #endif
    replaceExpression(node2, node9);
    SgNode *node10 = isSgNode(node9);
    return node10;
}

//r3 (const:c1 * const:c2) => (eval (const:c1 * const:c2))
SgNode* rule_r3(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal *node5 = isSgIntVal(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node3->get_rhs_operand();
    SgIntVal *node7 = isSgIntVal(node6);
    if (node7 == NULL) return NULL; 
    int node8 = node5->get_value();
    int node9 = node7->get_value();
    int node10 = node8 * node9;
    SgIntVal *node11 = buildIntVal(node10);
    #ifdef DEBUG
    cout << "Applying rule_r3" << endl;
    #endif
    replaceExpression(node2, node11);
    SgNode *node12 = isSgNode(node11);
    return node12;
}

//r4 (t:t1 * const:c1) => (const:c1 * t:t1)
SgNode* rule_r4(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgExpression *node5 = node3->get_rhs_operand();
    SgIntVal *node6 = isSgIntVal(node5);
    if (node6 == NULL) return NULL; 
    SgExpression *node7 = copy_expr(node6);
    SgExpression *node8 = copy_expr(node4);
    SgMultiplyOp *node9 = buildMultiplyOp(node7, node8);
    #ifdef DEBUG
    cout << "Applying rule_r4" << endl;
    #endif
    replaceExpression(node2, node9);
    SgNode *node10 = isSgNode(node9);
    return node10;
}

//r5 (const:c1 - const:c2) => (eval (const:c1 - const:c2))
SgNode* rule_r5(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgSubtractOp *node3 = isSgSubtractOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal *node5 = isSgIntVal(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node3->get_rhs_operand();
    SgIntVal *node7 = isSgIntVal(node6);
    if (node7 == NULL) return NULL; 
    int node8 = node5->get_value();
    int node9 = node7->get_value();
    int node10 = node8 - node9;
    SgIntVal *node11 = buildIntVal(node10);
    #ifdef DEBUG
    cout << "Applying rule_r5" << endl;
    #endif
    replaceExpression(node2, node11);
    SgNode *node12 = isSgNode(node11);
    return node12;
}

//r6 (t:t1 - const:c1) => ((- const:c1) + t:t1)
SgNode* rule_r6(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgSubtractOp *node3 = isSgSubtractOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgExpression *node5 = node3->get_rhs_operand();
    SgIntVal *node6 = isSgIntVal(node5);
    if (node6 == NULL) return NULL; 
    SgExpression *node7 = copy_expr(node6);
    SgMinusOp *node8 = buildMinusOp(node7);
    SgExpression *node9 = copy_expr(node4);
    SgAddOp *node10 = buildAddOp(node8, node9);
    #ifdef DEBUG
    cout << "Applying rule_r6" << endl;
    #endif
    replaceExpression(node2, node10);
    SgNode *node11 = isSgNode(node10);
    return node11;
}

//r7 (t:t1 + (t:t2 + t:t3)) => ((t:t1 + t:t2) + t:t3)
SgNode* rule_r7(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgAddOp *node3 = isSgAddOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgExpression *node5 = node3->get_rhs_operand();
    SgAddOp *node6 = isSgAddOp(node5);
    if (node6 == NULL) return NULL; 
    SgExpression *node7 = node6->get_lhs_operand();
    SgExpression *node8 = node6->get_rhs_operand();
    SgExpression *node9 = copy_expr(node4);
    SgExpression *node10 = copy_expr(node7);
    SgAddOp *node11 = buildAddOp(node9, node10);
    SgExpression *node12 = copy_expr(node8);
    SgAddOp *node13 = buildAddOp(node11, node12);
    #ifdef DEBUG
    cout << "Applying rule_r7" << endl;
    #endif
    replaceExpression(node2, node13);
    SgNode *node14 = isSgNode(node13);
    return node14;
}

//r8 (t:t1 * (t:t2 * t:t3)) => ((t:t1 * t:t2) * t:t3)
SgNode* rule_r8(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgExpression *node5 = node3->get_rhs_operand();
    SgMultiplyOp *node6 = isSgMultiplyOp(node5);
    if (node6 == NULL) return NULL; 
    SgExpression *node7 = node6->get_lhs_operand();
    SgExpression *node8 = node6->get_rhs_operand();
    SgExpression *node9 = copy_expr(node4);
    SgExpression *node10 = copy_expr(node7);
    SgMultiplyOp *node11 = buildMultiplyOp(node9, node10);
    SgExpression *node12 = copy_expr(node8);
    SgMultiplyOp *node13 = buildMultiplyOp(node11, node12);
    #ifdef DEBUG
    cout << "Applying rule_r8" << endl;
    #endif
    replaceExpression(node2, node13);
    SgNode *node14 = isSgNode(node13);
    return node14;
}

//r9 ((const:c1 + t:t1) + const:c2) => ((eval (const:c1 + const:c2)) + t:t1)
SgNode* rule_r9(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgAddOp *node3 = isSgAddOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgAddOp *node5 = isSgAddOp(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node5->get_lhs_operand();
    SgIntVal *node7 = isSgIntVal(node6);
    if (node7 == NULL) return NULL; 
    SgExpression *node8 = node5->get_rhs_operand();
    SgExpression *node9 = node3->get_rhs_operand();
    SgIntVal *node10 = isSgIntVal(node9);
    if (node10 == NULL) return NULL; 
    int node11 = node7->get_value();
    int node12 = node10->get_value();
    int node13 = node11 + node12;
    SgIntVal *node14 = buildIntVal(node13);
    SgExpression *node15 = copy_expr(node8);
    SgAddOp *node16 = buildAddOp(node14, node15);
    #ifdef DEBUG
    cout << "Applying rule_r9" << endl;
    #endif
    replaceExpression(node2, node16);
    SgNode *node17 = isSgNode(node16);
    return node17;
}

//r10 ((const:c1 * t:t1) * const:c2) => ((eval (const:c1 * const:c2)) * t:t1)
SgNode* rule_r10(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgMultiplyOp *node5 = isSgMultiplyOp(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node5->get_lhs_operand();
    SgIntVal *node7 = isSgIntVal(node6);
    if (node7 == NULL) return NULL; 
    SgExpression *node8 = node5->get_rhs_operand();
    SgExpression *node9 = node3->get_rhs_operand();
    SgIntVal *node10 = isSgIntVal(node9);
    if (node10 == NULL) return NULL; 
    int node11 = node7->get_value();
    int node12 = node10->get_value();
    int node13 = node11 * node12;
    SgIntVal *node14 = buildIntVal(node13);
    SgExpression *node15 = copy_expr(node8);
    SgMultiplyOp *node16 = buildMultiplyOp(node14, node15);
    #ifdef DEBUG
    cout << "Applying rule_r10" << endl;
    #endif
    replaceExpression(node2, node16);
    SgNode *node17 = isSgNode(node16);
    return node17;
}

//r11 ((const:c1 + t:t1) * const:c2) => ((eval (const:c1 * const:c2)) + (const:c2 * t:t1))
SgNode* rule_r11(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgAddOp *node5 = isSgAddOp(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node5->get_lhs_operand();
    SgIntVal *node7 = isSgIntVal(node6);
    if (node7 == NULL) return NULL; 
    SgExpression *node8 = node5->get_rhs_operand();
    SgExpression *node9 = node3->get_rhs_operand();
    SgIntVal *node10 = isSgIntVal(node9);
    if (node10 == NULL) return NULL; 
    int node11 = node7->get_value();
    int node12 = node10->get_value();
    int node13 = node11 * node12;
    SgIntVal *node14 = buildIntVal(node13);
    SgExpression *node15 = copy_expr(node10);
    SgExpression *node16 = copy_expr(node8);
    SgMultiplyOp *node17 = buildMultiplyOp(node15, node16);
    SgAddOp *node18 = buildAddOp(node14, node17);
    #ifdef DEBUG
    cout << "Applying rule_r11" << endl;
    #endif
    replaceExpression(node2, node18);
    SgNode *node19 = isSgNode(node18);
    return node19;
}

//r12 (const:c1 * (const:c2 + t:t1)) => ((eval (const:c1 * const:c2)) + (const:c1 * t:t1))
SgNode* rule_r12(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal *node5 = isSgIntVal(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node3->get_rhs_operand();
    SgAddOp *node7 = isSgAddOp(node6);
    if (node7 == NULL) return NULL; 
    SgExpression *node8 = node7->get_lhs_operand();
    SgIntVal *node9 = isSgIntVal(node8);
    if (node9 == NULL) return NULL; 
    SgExpression *node10 = node7->get_rhs_operand();
    int node11 = node5->get_value();
    int node12 = node9->get_value();
    int node13 = node11 * node12;
    SgIntVal *node14 = buildIntVal(node13);
    SgExpression *node15 = copy_expr(node5);
    SgExpression *node16 = copy_expr(node10);
    SgMultiplyOp *node17 = buildMultiplyOp(node15, node16);
    SgAddOp *node18 = buildAddOp(node14, node17);
    #ifdef DEBUG
    cout << "Applying rule_r12" << endl;
    #endif
    replaceExpression(node2, node18);
    SgNode *node19 = isSgNode(node18);
    return node19;
}

//r13 ((t:t1 + t:t2) * const:c1) => ((const:c1 * t:t1) + (const:c1 * t:t2))
SgNode* rule_r13(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgAddOp *node5 = isSgAddOp(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node5->get_lhs_operand();
    SgExpression *node7 = node5->get_rhs_operand();
    SgExpression *node8 = node3->get_rhs_operand();
    SgIntVal *node9 = isSgIntVal(node8);
    if (node9 == NULL) return NULL; 
    SgExpression *node10 = copy_expr(node9);
    SgExpression *node11 = copy_expr(node6);
    SgMultiplyOp *node12 = buildMultiplyOp(node10, node11);
    SgExpression *node13 = copy_expr(node9);
    SgExpression *node14 = copy_expr(node7);
    SgMultiplyOp *node15 = buildMultiplyOp(node13, node14);
    SgAddOp *node16 = buildAddOp(node12, node15);
    #ifdef DEBUG
    cout << "Applying rule_r13" << endl;
    #endif
    replaceExpression(node2, node16);
    SgNode *node17 = isSgNode(node16);
    return node17;
}

//r14 (const:c1 * (t:t1 + t:t2)) => ((const:c1 * t:t1) + (const:c1 * t:t2))
SgNode* rule_r14(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal *node5 = isSgIntVal(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node3->get_rhs_operand();
    SgAddOp *node7 = isSgAddOp(node6);
    if (node7 == NULL) return NULL; 
    SgExpression *node8 = node7->get_lhs_operand();
    SgExpression *node9 = node7->get_rhs_operand();
    SgExpression *node10 = copy_expr(node5);
    SgExpression *node11 = copy_expr(node8);
    SgMultiplyOp *node12 = buildMultiplyOp(node10, node11);
    SgExpression *node13 = copy_expr(node5);
    SgExpression *node14 = copy_expr(node9);
    SgMultiplyOp *node15 = buildMultiplyOp(node13, node14);
    SgAddOp *node16 = buildAddOp(node12, node15);
    #ifdef DEBUG
    cout << "Applying rule_r14" << endl;
    #endif
    replaceExpression(node2, node16);
    SgNode *node17 = isSgNode(node16);
    return node17;
}

//r15 ((t:t1 - t:t2) * const:c1) => ((const:c1 * t:t1) - (const:c1 * t:t2))
SgNode* rule_r15(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgSubtractOp *node5 = isSgSubtractOp(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node5->get_lhs_operand();
    SgExpression *node7 = node5->get_rhs_operand();
    SgExpression *node8 = node3->get_rhs_operand();
    SgIntVal *node9 = isSgIntVal(node8);
    if (node9 == NULL) return NULL; 
    SgExpression *node10 = copy_expr(node9);
    SgExpression *node11 = copy_expr(node6);
    SgMultiplyOp *node12 = buildMultiplyOp(node10, node11);
    SgExpression *node13 = copy_expr(node9);
    SgExpression *node14 = copy_expr(node7);
    SgMultiplyOp *node15 = buildMultiplyOp(node13, node14);
    SgSubtractOp *node16 = buildSubtractOp(node12, node15);
    #ifdef DEBUG
    cout << "Applying rule_r15" << endl;
    #endif
    replaceExpression(node2, node16);
    SgNode *node17 = isSgNode(node16);
    return node17;
}

//r16 (const:c1 * (t:t1 - t:t2)) => ((const:c1 * t:t1) - (const:c1 * t:t2))
SgNode* rule_r16(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal *node5 = isSgIntVal(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node3->get_rhs_operand();
    SgSubtractOp *node7 = isSgSubtractOp(node6);
    if (node7 == NULL) return NULL; 
    SgExpression *node8 = node7->get_lhs_operand();
    SgExpression *node9 = node7->get_rhs_operand();
    SgExpression *node10 = copy_expr(node5);
    SgExpression *node11 = copy_expr(node8);
    SgMultiplyOp *node12 = buildMultiplyOp(node10, node11);
    SgExpression *node13 = copy_expr(node5);
    SgExpression *node14 = copy_expr(node9);
    SgMultiplyOp *node15 = buildMultiplyOp(node13, node14);
    SgSubtractOp *node16 = buildSubtractOp(node12, node15);
    #ifdef DEBUG
    cout << "Applying rule_r16" << endl;
    #endif
    replaceExpression(node2, node16);
    SgNode *node17 = isSgNode(node16);
    return node17;
}

//r17 ((t:t1 + t:t2) * t:t3) => ((t:t1 * t:t3) + (t:t2 * t:t3))
SgNode* rule_r17(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgAddOp *node5 = isSgAddOp(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node5->get_lhs_operand();
    SgExpression *node7 = node5->get_rhs_operand();
    SgExpression *node8 = node3->get_rhs_operand();
    SgExpression *node9 = copy_expr(node6);
    SgExpression *node10 = copy_expr(node8);
    SgMultiplyOp *node11 = buildMultiplyOp(node9, node10);
    SgExpression *node12 = copy_expr(node7);
    SgExpression *node13 = copy_expr(node8);
    SgMultiplyOp *node14 = buildMultiplyOp(node12, node13);
    SgAddOp *node15 = buildAddOp(node11, node14);
    #ifdef DEBUG
    cout << "Applying rule_r17" << endl;
    #endif
    replaceExpression(node2, node15);
    SgNode *node16 = isSgNode(node15);
    return node16;
}

//r18 (t:t1 * (t:t2 + t:t3)) => ((t:t1 * t:t2) + (t:t1 * t:t3))
SgNode* rule_r18(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgExpression *node5 = node3->get_rhs_operand();
    SgAddOp *node6 = isSgAddOp(node5);
    if (node6 == NULL) return NULL; 
    SgExpression *node7 = node6->get_lhs_operand();
    SgExpression *node8 = node6->get_rhs_operand();
    SgExpression *node9 = copy_expr(node4);
    SgExpression *node10 = copy_expr(node7);
    SgMultiplyOp *node11 = buildMultiplyOp(node9, node10);
    SgExpression *node12 = copy_expr(node4);
    SgExpression *node13 = copy_expr(node8);
    SgMultiplyOp *node14 = buildMultiplyOp(node12, node13);
    SgAddOp *node15 = buildAddOp(node11, node14);
    #ifdef DEBUG
    cout << "Applying rule_r18" << endl;
    #endif
    replaceExpression(node2, node15);
    SgNode *node16 = isSgNode(node15);
    return node16;
}

//r19 ((t:t1 - t:t2) * t:t3) => ((t:t1 * t:t3) - (t:t2 * t:t3))
SgNode* rule_r19(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgSubtractOp *node5 = isSgSubtractOp(node4);
    if (node5 == NULL) return NULL; 
    SgExpression *node6 = node5->get_lhs_operand();
    SgExpression *node7 = node5->get_rhs_operand();
    SgExpression *node8 = node3->get_rhs_operand();
    SgExpression *node9 = copy_expr(node6);
    SgExpression *node10 = copy_expr(node8);
    SgMultiplyOp *node11 = buildMultiplyOp(node9, node10);
    SgExpression *node12 = copy_expr(node7);
    SgExpression *node13 = copy_expr(node8);
    SgMultiplyOp *node14 = buildMultiplyOp(node12, node13);
    SgSubtractOp *node15 = buildSubtractOp(node11, node14);
    #ifdef DEBUG
    cout << "Applying rule_r19" << endl;
    #endif
    replaceExpression(node2, node15);
    SgNode *node16 = isSgNode(node15);
    return node16;
}

//r20 (t:t1 * (t:t2 - t:t3)) => ((t:t1 * t:t2) - (t:t1 * t:t3))
SgNode* rule_r20(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgExpression *node5 = node3->get_rhs_operand();
    SgSubtractOp *node6 = isSgSubtractOp(node5);
    if (node6 == NULL) return NULL; 
    SgExpression *node7 = node6->get_lhs_operand();
    SgExpression *node8 = node6->get_rhs_operand();
    SgExpression *node9 = copy_expr(node4);
    SgExpression *node10 = copy_expr(node7);
    SgMultiplyOp *node11 = buildMultiplyOp(node9, node10);
    SgExpression *node12 = copy_expr(node4);
    SgExpression *node13 = copy_expr(node8);
    SgMultiplyOp *node14 = buildMultiplyOp(node12, node13);
    SgSubtractOp *node15 = buildSubtractOp(node11, node14);
    #ifdef DEBUG
    cout << "Applying rule_r20" << endl;
    #endif
    replaceExpression(node2, node15);
    SgNode *node16 = isSgNode(node15);
    return node16;
}

//r21 (- const:c1) => (eval (- const:c1))
SgNode* rule_r21(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMinusOp *node3 = isSgMinusOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_operand();
    SgIntVal *node5 = isSgIntVal(node4);
    if (node5 == NULL) return NULL; 
    int node6 = node5->get_value();
    int node7 = -node6;
    SgIntVal *node8 = buildIntVal(node7);
    #ifdef DEBUG
    cout << "Applying rule_r21" << endl;
    #endif
    replaceExpression(node2, node8);
    SgNode *node9 = isSgNode(node8);
    return node9;
}

//r22 (1 * t:t1) => t:t1
SgNode* rule_r22(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal * node5 = isSgIntVal(node4); 
    if (node5 == NULL) return NULL; 
    int node6 = node5->get_value();
    if (node6 != 1) return NULL;
    SgExpression *node7 = node3->get_rhs_operand();
    SgExpression *node8 = copy_expr(node7);
    #ifdef DEBUG
    cout << "Applying rule_r22" << endl;
    #endif
    replaceExpression(node2, node8);
    SgNode *node9 = isSgNode(node8);
    return node9;
}

//r23 (0 * t:t1) => 0
SgNode* rule_r23(SgNode *node1)
{
    SgExpression *node2 = isSgExpression(node1);
    SgMultiplyOp *node3 = isSgMultiplyOp(node1);
    if (node3 == NULL) return NULL; 
    SgExpression *node4 = node3->get_lhs_operand();
    SgIntVal * node5 = isSgIntVal(node4); 
    if (node5 == NULL) return NULL; 
    int node6 = node5->get_value();
    if (node6 != 0) return NULL;
    SgExpression *node7 = node3->get_rhs_operand();
    SgIntVal *node8 = buildIntVal(0);
    #ifdef DEBUG
    cout << "Applying rule_r23" << endl;
    #endif
    replaceExpression(node2, node8);
    SgNode *node9 = isSgNode(node8);
    return node9;
}
int rule_count = 23;
p_rulefunc rule_funcs[23] = {
&rule_r1
,&rule_r2
,&rule_r3
,&rule_r4
,&rule_r5
,&rule_r6
,&rule_r7
,&rule_r8
,&rule_r9
,&rule_r10
,&rule_r11
,&rule_r12
,&rule_r13
,&rule_r14
,&rule_r15
,&rule_r16
,&rule_r17
,&rule_r18
,&rule_r19
,&rule_r20
,&rule_r21
,&rule_r22
,&rule_r23
};
